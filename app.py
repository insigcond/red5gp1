"""
Esta es una prueba de la rama dev:
"""

from flask import Flask, render_template, redirect, url_for, flash, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import datetime
from sqlalchemy import DateTime
import datetime

app = Flask(__name__)

# settings
app.secret_key = "william"

"""nos conectamos a la bd"""
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql+pymysql://william:123456789@192.168.239.128/PR5g"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# settings
app.secret_key = "william"

"""iniciamos trabajo con la bd"""
db = SQLAlchemy(app)
ma = Marshmallow(app)


"""Creación de los modelos"""

"""
Articulo de Comida
"""


class Articulo(db.Model):
    __tablename__ = "Articulo"
    id = db.Column(db.Integer, primary_key=True)
    Nombre = db.Column(db.String(70), unique=True)
    Precio = db.Column(db.Float())

    def __init__(self, var_Nombre, var_Precio):
        self.Nombre = var_Nombre
        self.Precio = var_Precio


db.create_all()


class ArticulosSchema(ma.Schema):
    class Meta:
        fields = ("id", "Nombre", "Precio")


"""
Cliente
"""


class Cliente(db.Model):
    __tablename__ = "Cliente"
    Id = db.Column(db.Integer, primary_key=True)
    Identificacion = db.Column(db.Integer)
    Nombre = db.Column(db.String(70), unique=True)
    Direccion = db.Column(db.String(70))
    Ciudad = db.Column(db.String(70))
    Telefono = db.Column(db.String(70))

    def __init__(
        self, var_Identificacion, var_Nombre, var_Direccion, var_Ciudad, var_Telefono
    ):
        self.Identificacion = var_Identificacion
        self.Nombre = var_Nombre
        self.Direccion = var_Direccion
        self.Ciudad = var_Ciudad
        self.Telefono = var_Telefono


db.create_all()


class ClientesSchema(ma.Schema):
    class Meta:
        fields = ("Id", "Identificacion", "Nombre", "Direccion", "Ciudad", "Telefono")


"""
Pedidos
"""


class Pedido(db.Model):
    __tablename__ = "Pedido"
    Id = db.Column(db.Integer, primary_key=True)
    Id_Cliente = db.Column(db.Integer, db.ForeignKey("Cliente.Id"))
    Cliente = db.relationship("Cliente", backref=db.backref("Pedidos", lazy=True))

    FHPedido = db.Column("FechaHoraP", DateTime, default=datetime.datetime.utcnow)
    # creamos la relación#
    Id_Articulos = db.Column(db.Integer, db.ForeignKey("Articulo.id"))
    Articulo = db.relationship("Articulo", backref=db.backref("pedidos", lazy=True))
    Cantidad = db.Column(db.Integer)
    ValorTotal = db.Column(db.Float())
    Estado = db.Column(db.String(70))

    def __init__(
        self, var_IdCliente, var_IdArticulos, var_Cantidad, var_ValorTotal, var_Estado
    ):
        self.Id_Cliente = var_IdCliente
        # self.FHPedido = var_FHPedido
        self.Id_Articulos = var_IdArticulos
        self.Cantidad = var_Cantidad
        self.ValorTotal = var_ValorTotal
        self.Estado = var_Estado
        # self.Articulo = Articulo


class PedidosSchema(ma.Schema):
    class Meta:
        fields = (
            "Id",
            "Id_Cliente",
            "FechaHoraP",
            "Id_Articulos",
            "Cantidad",
            "ValorTotal",
            "Estado",
        )


db.create_all()

"""
Estado del Pedido
"""


class EstadoPedido(db.Model):
    __tablename__ = "EstadoPedido"
    Id = db.Column(db.Integer, primary_key=True)
    Id_Pedido = db.Column(db.Integer, db.ForeignKey("Pedido.Id"))
    Pedido = db.relationship("Pedido", backref=db.backref("EstadoPedido", lazy=True))
    FHPedido = db.Column("FechaHoraP", DateTime, default=datetime.datetime.utcnow)
    Estado = db.Column(db.String(70))

    def __init__(self, var_Id_Pedido, var_Estado):
        self.Id_Pedido = var_Id_Pedido
        # self.FHPedido = var_FHPedido
        self.Estado = var_Estado


class EstadoPedidosSchema(ma.Schema):
    class Meta:
        fields = ("Id", "Id_Pedido", "FHPedido", "Estado")


db.create_all()


Articulo_schema = ArticulosSchema()
Articulos_schema = ArticulosSchema(many=True)
Cliente_schema = ClientesSchema()
Clientes_schema = ClientesSchema(many=True)
Pedido_schema = PedidosSchema()
Pedidos_schema = PedidosSchema(many=True)
EstadoPedido_schema = EstadoPedidosSchema()
EstadoPedidos_schema = EstadoPedidosSchema(many=True)


"""
API
"""

"""
Principal 
"""


@app.route("/", methods=["GET"])
def index():

    return render_template("card.html")


"""
Cliente
"""


@app.route("/Cliente", methods=["GET"])
def indexcliente():

    all_cliente = Cliente.query.all()
    resultclientes = Clientes_schema.dump(all_cliente)
    return render_template("Cliente.html", clientes=resultclientes)


@app.route("/addCliente", methods=["GET", "POST"])
def addCliente():
    if request.method == "POST":

        var_Identificacion = request.form["Identificacion"]
        var_Nombre = request.form["Nombre"]
        var_Direccion = request.form["Direccion"]
        var_Ciudad = request.form["Ciudad"]
        var_Telefono = request.form["Telefono"]
        NewCliente = Cliente(
            var_Identificacion, var_Nombre, var_Direccion, var_Ciudad, var_Telefono
        )
        db.session.add(NewCliente)
        db.session.commit()
        flash("Cliente Agregado con Exito")
        return redirect(url_for("indexcliente"))


"""
Articulo
"""


@app.route("/Articulo", methods=["GET"])
def indexArticulo():
    all_articulo = Articulo.query.all()
    resultarticulo = Articulos_schema.dump(all_articulo)
    return render_template("Articulo.html", articulos=resultarticulo)


@app.route("/addArticulo", methods=["GET", "POST"])
def addArticulo():
    if request.method == "POST":

        var_Nombre = request.form["Nombre"]
        var_Precio = request.form["Precio"]

        NewArticulo = Articulo(var_Nombre, var_Precio)
        db.session.add(NewArticulo)
        db.session.commit()
        flash("Articulo Agregado con Exito")
        return redirect(url_for("indexArticulo"))


"""
Pedido
"""


@app.route("/Pedido", methods=["GET"])
def indexPedido():
    all_cliente = Cliente.query.all()
    resultclientes = Clientes_schema.dump(all_cliente)
    all_articulo = Articulo.query.all()
    resultarticulo = Articulos_schema.dump(all_articulo)
    all_Pedido = Pedido.query.all()
    resultPedidos = Pedidos_schema.dump(all_Pedido)
    all_Ep = EstadoPedido.query.all()
    resultEp = EstadoPedidos_schema.dump(all_Ep)
    # recorremos el Pedido
    UltimoEstado = ""
    NewEstadoPedido = {}
    for itempedido in all_Pedido:
        for itemestado in all_Ep:
            if itempedido.Id == itemestado.Id_Pedido:
                UltimoEstado = itemestado.Estado
        NewEstadoPedido[itempedido.Id] = UltimoEstado
        UltimoEstado = ""
        
    print(NewEstadoPedido)
    return render_template(
        "Pedido.html",
        clientes=resultclientes,
        articulos=resultarticulo,
        pedidos=resultPedidos,
        Ep=resultEp,
        neps=NewEstadoPedido,
    )


@app.route("/addPedido", methods=["GET", "POST"])
def addPedido():
    if request.method == "POST":

        var_Id_Cliente = request.form["Id_Cliente"]
        var_Id_Articulo = request.form["Id_Articulo"]

        var_Cantidad = request.form["Cantidad"]
        var_ValorTotal = 0
        var_Estado = "EN ESPERA"
        NewPedido = Pedido(
            var_Id_Cliente, var_Id_Articulo, var_Cantidad, var_ValorTotal, var_Estado
        )
        db.session.add(NewPedido)
        db.session.commit()

        flash("Pedido Agregado con Exito")
        return redirect(url_for("indexPedido"))


"""
Estdo del Pedido
"""


@app.route("/Historico/<Id>", methods=["GET"])
def EP(Id):
    if int(Id) >= 1:
        all_Ep = EstadoPedido.query.filter(EstadoPedido.Id_Pedido.like(Id)).all()
        resultEp = EstadoPedidos_schema.dump(all_Ep)
        [print(resultEp) for item in resultEp]
        return render_template("EstadoPedido.html", EstadoPedidos=resultEp)
    else:
        all_Ep = EstadoPedido.query.all()
        resultEp = EstadoPedidos_schema.dump(all_Ep)
        return render_template("EstadoPedido.html", EstadoPedidos=resultEp)


@app.route("/cambioE/<Id>", methods=["POST", "GET"])
def getPedido(Id):
    if request.method == "GET":

        pedido = Pedido.query.get(Id)
        ep = EstadoPedido.query.filter(EstadoPedido.Id_Pedido.like(Id)).all()
        if pedido.Estado == "EN ESPERA":
            pedido.Estado = "ir a estado"
            db.session.commit()
            var_Estado = "EN PREPARACIÓN"
            NewEstado = EstadoPedido(Id, var_Estado)
            db.session.add(NewEstado)
            db.session.commit()
            return redirect(url_for("indexPedido"))
        else:

            for item in ep:
                if item.Estado == "EN PREPARACIÓN":
                    var_Estado = "EN REPARTO"

                elif item.Estado == "EN REPARTO":
                    var_Estado = "ENTREGADO"

                else:
                    return redirect(url_for("indexPedido"))
            NewEstado = EstadoPedido(Id, var_Estado)
            db.session.add(NewEstado)
            db.session.commit()
            return redirect(url_for("indexPedido"))


# Articulos

# Iniciamos app para que se ejecute en un puerto#

if __name__ == "__main__":

    app.run(debug=True)
